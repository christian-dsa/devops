# Objective: Find tool option to get as close as possible to Autosar C++ rules.
# Current state: Will use Clang and Clang-tidy with a maximum of checks. Before removing a check, will
# verify that it is not present in the list here.

# Autosar C++ 14
# Language independent issues
# Unnecessary Constructs
Rule M0-1-1 (required, implementation, automated)
A project shall not contain unreachable code.
Tool: clang -Wunreachable-code-aggressive

Rule M0-1-2 (required, implementation, automated)
A project shall not contain infeasible paths.
Tool: clang -Wunreachable-code-aggressive? CLion Data flow analysis | Constant condition

Rule M0-1-3 (required, implementation, automated)
A project shall not contain unused variables.
Tool: clang -Wunused-variable, CLion Data flow analysis | Unused local variable

Rule M0-1-4 (required, implementation, automated)
A project shall not contain non-volatile Plain Old Data variables having only one use.
Tool: clang -Wunused-but-set-variable, CLion Data flow analysis | Unused value

Rule A0-1-1 (required, implementation, automated)
A project shall not contain instances of non-volatile variables being given
values that are not subsequently used.
Tool: clang-tidy clang-analyzer-deadcode.DeadStores

Exception
Loop control variables (see Section 6.6.5) are exempt from this rule.

Rule A0-1-2 (required, implementation, automated)
The value returned by a function having a non-void return type that is not an
overloaded operator shall be used.
Tool: clang -Wunused-result, clang-tidy bugprone-unused-return-value

Exception
The return value of a function call may be discarded by use of a static_cast<void> cast,
so intentions of a programmer are explicitly stated.

HIC++ v4.0 [9]: 17.5.1 Do not ignore the result of std::remove, std::remove_if or
std::unique.

Rule M0-1-8 (required, implementation, automated)
All functions with void return type shall have external side effect(s).
Tool: None

Rule M0-1-9 (required, implementation, automated)
There shall be no dead code.
Tool: clang-tidy clang-analyzer-deadcode.DeadStores, Gcov, clang -fprofile-instr-generate -fcoverage-mapping

Rule M0-1-10 (advisory, implementation, automated)
Every defined function should be called at least once.
Tool: clang -Wunused-function

Rule A0-1-3 (required, implementation, automated)
Every function defined in an anonymous namespace, or static function with
internal linkage, or private member function shall be used.
Tool: clang -Wunused-function 

Rule A0-1-4 (required, implementation, automated)
There shall be no unused named parameters in non-virtual functions.
Tool: clang -Wunused-parameter, -Wunused-but-set-parameter

Note: This rule does not apply to unnamed parameters, as they are widely used in
SFINAE and concept compliance. SFINAE (Substitution failure is not an error).

Rule A0-1-5 (required, implementation, automated)
There shall be no unused named parameters in the set of parameters for a
virtual function and all the functions that override it.
Tool: clang -Wunused-parameter, -Wunused-but-set-parameter

Rule A0-1-6 (advisory, implementation, automated)
There should be no unused type declarations.
Tool: clang -Wunused-local-typedef

# Storage
Rule M0-2-1 (required, implementation, automated)
An object shall not be assigned to an overlapping object.
Tool: None

# Runtime failures
Rule M0-3-1 (required, implementation / verification, non-automated)
Minimization of run-time failures shall be ensured by the use of at least one
of: (a) static analysis tools/techniques; (b) dynamic analysis tools/techniques;
(c) explicit coding of checks to handle run-time faults.
Tool:

Rule M0-3-2 (required, implementation, non-automated)
If a function generates error information, then that error information shall be
tested.
Tool:

# Arithmetic
Rule M0-4-1 (required, implementation, non-automated)
Use of scaled-integer or fixed-point arithmetic shall be documented.
Tool:

Rule M0-4-2 (required, implementation, non-automated)
Use of floating-point arithmetic shall be documented.
Tool:

Rule A0-4-1 (required, infrastructure / toolchain, non-automated)
Floating-point implementation shall comply with IEEE 754 standard.
Tool:

Rule A0-4-2 (required, implementation, automated)
Type long double shall not be used.
Tool: None

Rule A0-4-3 (required, toolchain, automated)
The implementations in the chosen compiler shall strictly comply with the
C++14 Language Standard.
Tool:

Rule A0-4-4 (required, implementation, partially automated)
Range, domain and pole errors shall be checked when using math
functions.
Tool: None

# General
# Scope
Rule A1-1-1 (required, implementation, automated)
All code shall conform to ISO/IEC 14882:2014 - Programming Language C++
and shall not use deprecated features.
Tool: clang-tidy modernize-deprecated-headers clang-analyzer-security.insecureAPI.DeprecatedOrUnsafeBufferHandling, clang -Wdeprecated

Rule M1-0-2 (required, toolchain, non-automated)
Multiple compilers shall only be used if they have a common, defined
interface.
Tool:

Rule A1-1-2 (required, implementation / toolchain, non-automated)
A warning level of the compilation process shall be set in compliance with
project policies.
Tool:

Rule A1-1-3 (required, toolchain, non-automated)
An optimization option that disregards strict standard compliance shall not
be turned on in the chosen compiler.
Tool:

# Normative references
Rule A1-2-1 (required, toolchain, non-automated)
When using a compiler toolchain (including preprocessor, compiler itself,
linker, C++ standard libraries) in safety-related software, the tool confidence
level (TCL) shall be determined. In case of TCL2 or TCL3, the compiler shall
undergo a “Qualification of a software tool”, as per ISO 26262-8.11.4.6 [6].
Tool:

# Implementation compliance
Rule A1-4-1 (required, implementation / verification, non-automated)
Code metrics and their valid boundaries shall be defined and code shall
comply with defined boundaries of code metrics.
Tool:

Rule A1-4-3 (advisory, implementation, automated)
All code should compile free of compiler warnings.
Tool: clang -Werror

# Lexical conventions
# Character sets
Rule A2-3-1 (required, architecture / design / implementation, automated)
Only those characters specified in the C++ Language Standard basic source
character set shall be used in the source code.
Tool:

Exception
It is permitted to use other characters inside the text of a wide string and a UTF-8
encoded string literal.
It is also permitted to use a character @ inside comments. See rule A2-7-3.

# Alternative tokens
Rule A2-5-1 (required, implementation, automated)
Trigraphs shall not be used. The Trigraphs are: ??=, ??/, ??’, ??(, ??), ??!, ??<, ??>, ??-.
Tool: clang -Wtrigraphs

Rule A2-5-2 (required, implementation, automated)
Digraphs shall not be used. The digraphs are: <%, %>, <:, :>, %:, %:%:.
Tool: None

# Comments
Rule M2-7-1 (required, implementation, automated)
The character sequence /* shall not be used within a C-style comment.
Tool:

Rule A2-7-1 (required, implementation, automated)
The character \ shall not occur as a last character of a C++ comment.
Tool:

Rule A2-7-2 (required, implementation, non-automated)
Sections of code shall not be “commented out”.
Tool:

Rule A2-7-3 (required, implementation, automated)
All declarations of “user-defined” types, static and non-static data
members, functions and methods shall be preceded by documentation.
Tool:

Rule A2-7-5 (required, implementation, non-automated)
Comments shall not document any actions or sources (e.g. tables, figures,
paragraphs, etc.) that are outside of the file.
Tool:

# Header names
Rule A2-8-1 (required, architecture / design / implementation, non-automated)
A header file name should reflect the logical entity for which it provides
declarations.
Tool:

Rule A2-8-2 (advisory, architecture / design / implementation, non-automated)
An implementation file name should reflect the logical entity for which it
provides definitions.
Tool:

# Identifiers
Rule M2-10-1 (required, architecture / design / implementation, automated)
Different identifiers shall be typographically unambiguous.
Tool: clang-tidy misc-confusable-identifiers misc-misleading-identifier 

Rule A2-10-1 (required, architecture / design / implementation, automated)
An identifier declared in an inner scope shall not hide an identifier declared
in an outer scope.
Tool: clang -Wshadow

Rule A2-10-6 (required, implementation, automated)
A class or enumeration name shall not be hidden by a variable, function or
enumerator declaration in the same scope.
Tool: clang -Wshadow

Rule A2-10-4 (required, implementation, automated)
The identifier name of a non-member object with static storage duration or
static function shall not be reused within a namespace.
Tool: clang -Wshadow

Rule A2-10-5 (advisory, design / implementation, automated)
An identifier name of a function with static storage duration or a
non-member object with external or internal linkage should not be reused.
Tool: clang -Wshadow

# Keywords
Rule A2-11-1 (required, design / implementation, automated)
Volatile keyword shall not be used.
Tool: 

Note: The main intention of this rule is to eliminate incorrect usages of volatile keyword
and force developers to precisely document each usage of volatile keyword.

# Literals
Rule A2-13-1 (required, architecture / design / implementation, automated)
Only those escape sequences that are defined in ISO/IEC 14882:2014 shall
be used.
Tool: clang -Wunknown-escape-sequence

Rule A2-13-6 (required, architecture / design / implementation, automated)
Universal character names shall be used only inside character or string
literals.
Tool: clang -Wunicode -Wunicode-homoglyph -Wunicode-whitespace -Wunicode-zero-width

Rule A2-13-5 (advisory, implementation, automated)
Hexadecimal constants should be upper case.
Tool: None

Rule M2-13-2 (required, architecture / design / implementation, automated)
Octal constants (other than zero) and octal escape sequences (other than
“\0” ) shall not be used.
Tool: None

Rule M2-13-3 (required, architecture / design / implementation, automated)
A “U” suffix shall be applied to all octal or hexadecimal integer literals of
unsigned type.
Tool: Will only apply for #define.

Rule M2-13-4 (required, architecture / design / implementation, automated)
Literal suffixes shall be upper case.
Tool: Will only apply for #define.

Rule A2-13-2 (required, implementation, automated)
String literals with different encoding prefixes shall not be concatenated.
Tool:

Rule A2-13-3 (required, architecture / design / implementation, automated)
Type wchar_t shall not be used.
Tool: None

Rule A2-13-4 (required, architecture / design / implementation, automated)
String literals shall not be assigned to non-constant pointers.
Tool: C++ standard

# Basic concepts
# Declarations and definitions  
Rule A3-1-1 (required, architecture / design / implementation, automated)
It shall be possible to include any header file in multiple translation units
without violating the One Definition Rule.
Tool: 

Rule A3-1-2 (required, architecture / design / implementation, automated)
Header files, that are defined locally in the project, shall have a file name
extension of one of: ".h", ".hpp" or ".hxx".
Tool: CLion

Rule A3-1-3 (advisory, architecture / design / implementation, automated)
Implementation files, that are defined locally in the project, should have a
file name extension of ".cpp".
Tool: CLion

Rule M3-1-2 (required, implementation, automated)
Functions shall not be declared at block scope.
Tool: None

Rule A3-1-4 (required, design / implementation, automated)
When an array with external linkage is declared, its size shall be stated
explicitly.
Tool: No C style array. clang-tidy modernize-avoid-c-arrays

Rule A3-1-5 (required, design, partially-automated)
A function definition shall only be placed in a class definition if (1) the
function is intended to be inlined (2) it is a member function template (3) it
is a member function of a class template.
Tool: None

Rule A3-1-6 (advisory, design, automated)
Trivial accessor and mutator functions should be inlined.
Tool: None

# One Definition Rule
Rule M3-2-1 (required, implementation, automated)
All declarations of an object or function shall have compatible types.
Tool: clang -Wodr

Rule M3-2-2 (required, implementation, automated)
The One Definition Rule shall not be violated.
Tool: clang -Wodr

Rule M3-2-3 (required, implementation, automated)
A type, object or function that is used in multiple translation units shall be
declared in one and only one file.
Tool: clang -Wodr

Rule M3-2-4 (required, implementation, automated)
An identifier with external linkage shall have exactly one definition.
Tool: clang -Wodr

# Scope
Rule A3-3-1 (required, implementation, automated)
Objects or functions with external linkage (including members of named
namespaces) shall be declared in a header file.
Tool: None

Rule A3-3-2 (required, implementation, automated)
Static and thread-local objects shall be constant-initialized.
Tool: clang-tidy fuchsia-statically-constructed-objects

Rule M3-3-2 (required, implementation, automated)
If a function has internal linkage then all re-declarations shall include the
static storage class specifier.
Tool: clang-tidy readability-convert-member-functions-to-static

# Name lookup
Rule M3-4-1 (required, implementation, automated)
An identifier declared to be an object or type shall be defined in a block that
minimizes its visibility.
Tool: None

# Object lifetime
Rule A3-8-1 (required, implementation, not automated)
An object shall not be accessed outside its lifetime.
Tool: 

# Types
Rule M3-9-1 (required, implementation, automated)
The types used for an object, a function return type, or a function parameter
shall be token-for-token identical in all declarations and re-declarations.
Tool: 

Rule A3-9-1 (required, implementation, automated)
Fixed width integer types from <cstdint>, indicating the size and
signedness, shall be used in place of the basic numerical types.
Tool: 

Rule M3-9-3 (required, implementation, automated)
The underlying bit representations of floating-point values shall not be
used.
Tool:

# Standard conversions
# Integral promotions
Rule M4-5-1 (required, implementation, automated)
Expressions with type bool shall not be used as operands to built-in
operators other than the assignment operator =, the logical operators
&&, ||, !, the equality operators == and ! =, the unary & operator, and the
conditional operator.
Tool: CLion MISRA C++ check, clang-tidy readability-implicit-bool-conversion

Rule A4-5-1 (required, implementation, automated)
Expressions with type enum or enum class shall not be used as operands
to built-in and overloaded operators other than the subscript operator [ ],
the assignment operator =, the equality operators == and ! =, the unary &
operator, and the relational operators <, <=, >, >=.
Tool: clang-tidy bugprone-suspicious-enum-usage

Rule M4-5-3 (required, implementation, automated)
Expressions with type (plain) char and wchar_t shall not be used as
operands to built-in operators other than the assignment operator =, the
equality operators == and ! =, and the unary & operator.
Tool:

# Integral conversion
Rule A4-7-1 (required, implementation, automated)
An integer expression shall not lead to data loss.
Tool: clang -Wnarrowing clang-tidy cppcoreguidelines-narrowing-conversions

# Pointer conversions
Rule M4-10-1 (required, implementation, automated)
NULL shall not be used as an integer value.
Tool: clang-tidy modernize-use-nullptr

Rule A4-10-1 (required, architecture / design / implementation, automated)
Only nullptr literal shall be used as the null-pointer-constant.
Tool: clang-tidy modernize-use-nullptr

Rule M4-10-2 (required, implementation, automated)
Literal zero (0) shall not be used as the null-pointer-constant.
Tool: clang-tidy modernize-use-nullptr

# Expressions                                                   ********************************************************
# General

Rule A5-0-1 (required, implementation, automated)
The value of an expression shall be the same under any order of evaluation
that the standard permits.
Tool: 

Rule M5-0-2 (advisory, implementation, partially automated)
Limited dependence should be placed on C++ operator precedence rules in
expressions.
Tool:

Rule M5-0-3 (required, implementation, automated)
A cvalue expression shall not be implicitly converted to a different
underlying type.
Tool:

Rule M5-0-4 (required, implementation, automated)
An implicit integral conversion shall not change the signedness of the
underlying type.
Tool: 

Rule M5-0-5 (required, implementation, automated)
There shall be no implicit floating-integral conversions.
Tool:

Rule M5-0-6 (required, implementation, automated)
An implicit integral or floating-point conversion shall not reduce the size of
the underlying type.
Tool:

Rule M5-0-7 (required, implementation, automated)
There shall be no explicit floating-integral conversions of a cvalue
expression.
Tool:

Rule M5-0-8 (required, implementation, automated)
An explicit integral or floating-point conversion shall not increase the size
of the underlying type of a cvalue expression.
Tool:

Rule M5-0-9 (required, implementation, automated)
An explicit integral conversion shall not change the signedness of the
underlying type of a cvalue expression.
Tool:

Rule M5-0-10 (required, implementation, automated)
If the bitwise operators ~and << are applied to an operand with an
underlying type of unsigned char or unsigned short, the result shall be
immediately cast to the underlying type of the operand.
Tool:

Rule M5-0-11 (required, implementation, automated)
The plain char type shall only be used for the storage and use of character
values.
Tool:

Rule M5-0-12 (required, implementation, automated)
Signed char and unsigned char type shall only be used for the storage and
use of numeric values.
Tool:

Rule A5-0-2 (required, implementation, automated)
The condition of an if-statement and the condition of an iteration statement
shall have type bool.
Tool:

Rule M5-0-14 (required, implementation, automated)
The first operand of a conditional-operator shall have type bool.
Tool:

Rule M5-0-15 (required, implementation, automated)
Array indexing shall be the only form of pointer arithmetic.
Tool:

Rule M5-0-16 (required, implementation, automated)
A pointer operand and any pointer resulting from pointer arithmetic using
that operand shall both address elements of the same array.
Tool:

Rule M5-0-17 (required, implementation, automated)
Subtraction between pointers shall only be applied to pointers that address
elements of the same array.
Tool:

Rule A5-0-4 (required, implementation, automated)
Pointer arithmetic shall not be used with pointers to non-final classes.
Tool:

Rule M5-0-18 (required, implementation, automated)
'>, >=, <, <=' shall not be applied to objects of pointer type, except where
they point to the same array.
Tool:

Rule A5-0-3 (required, implementation, automated)
The declaration of objects shall contain no more than two levels of pointer
indirection.
Tool:

Rule M5-0-20 (required, implementation, automated)
Non-constant operands to a binary bitwise operator shall have the same
underlying type.
Tool:

Rule M5-0-21 (required, implementation, automated)
Bitwise operators shall only be applied to operands of unsigned underlying
type.
Tool:

# Primary expression
Rule A5-1-1 (required, implementation, partially automated)
Literal values shall not be used apart from type initialization, otherwise
symbolic names shall be used instead.
Tool:

Rule A5-1-2 (required, implementation, automated)
Variables shall not be implicitly captured in a lambda expression.
Tool:

Rule A5-1-3 (required, implementation, automated)
Parameter list (possibly empty) shall be included in every lambda
expression.
Tool:

Rule A5-1-4 (required, implementation, automated)
A lambda expression object shall not outlive any of its reference-captured
objects.
Tool:

Rule A5-1-6 (advisory, implementation, automated)
Return type of a non-void return type lambda expression should be
explicitly specified.
Tool:

Rule A5-1-7 (required, implementation, automated)
A lambda shall not be an operand to decltype or typeid.
Tool:

Rule A5-1-8 (advisory, implementation, automated)
Lambda expressions should not be defined inside another lambda
expression.
Tool:

Rule A5-1-9 (advisory, implementation, automated)
Identical unnamed lambda expressions shall be replaced with a named
function or a named lambda expression.
Tool:

# Postfix expressions
Rule M5-2-2 (required, implementation, automated)
A pointer to a virtual base class shall only be cast to a pointer to a derived
class by means of dynamic_cast.
Tool:

Rule M5-2-3 (advisory, implementation, automated)
Casts from a base class to a derived class should not be performed on
polymorphic types.
Tool:

Rule A5-2-1 (advisory, implementation, automated)
dynamic_cast should not be used.
Tool:

Rule A5-2-2 (required, implementation, automated)
Traditional C-style casts shall not be used.
Tool:

Rule A5-2-3 (required, implementation, automated)
A cast shall not remove any const or volatile qualification from the type of a
pointer or reference.
Tool:

Rule M5-2-6 (required, implementation, automated)
A cast shall not convert a pointer to a function to any other pointer type,
including a pointer to function type.
Tool:

Rule A5-2-4 (required, implementation, automated)
reinterpret_cast shall not be used.
Tool:

Rule A5-2-6 (required, implementation, automated)
The operands of a logical && or \\ shall be parenthesized if the operands
contain binary operators.
Tool:

Rule M5-2-8 (required, implementation, automated)
An object with integer type or pointer to void type shall not be converted to
an object with pointer type.
Tool:

Rule M5-2-9 (required, implementation, automated)
A cast shall not convert a pointer type to an integral type.
Tool:

Rule M5-2-10 (required, implementation, automated)
The increment (++) and decrement (−−) operators shall not be mixed with
other operators in an expression.
Tool:

Rule M5-2-11 (required, implementation, automated)
The comma operator, && operator and the || operator shall not be
overloaded.
Tool:

Rule A5-2-5 (required, implementation, automated)
An array or container shall not be accessed beyond its range.
Tool:

Rule M5-2-12 (required, implementation, automated)
An identifier with array type passed as a function argument shall not decay
to a pointer.
Tool:

# Unary expressions
Rule M5-3-1 (required, implementation, automated)
Each operand of the ! operator, the logical && or the logical || operators
shall have type bool.
Tool:

Rule M5-3-2 (required, implementation, automated)
The unary minus operator shall not be applied to an expression whose
underlying type is unsigned.
Tool:

Rule M5-3-3 (required, implementation, automated)
The unary & operator shall not be overloaded.
Tool:

Rule M5-3-4 (required, implementation, automated)
Evaluation of the operand to the sizeof operator shall not contain side
effects.
Tool:

Rule A5-3-1 (required, implementation, non-automated)
Evaluation of the operand to the typeid operator shall not contain side
effects.
Tool:

Rule A5-3-2 (required, implementation, partially automated)
Null pointers shall not be dereferenced.
Tool:

Rule A5-3-3 (required, implementation, automated)
Pointers to incomplete class types shall not be deleted.
Tool:

# Pointer-to-member
Rule A5-5-1 (required, implementation, automated)
A pointer to member shall not access non-existent class members.
Tool:

# Multiplicative operators
Rule A5-6-1 (required, implementation, automated)
The right hand operand of the integer division or remainder operators shall
not be equal to zero.
Tool:

# Shift operators
Rule M5-8-1 (required, implementation, partially automated)
The right hand operand of a shift operator shall lie between zero and one
less than the width in bits of the underlying type of the left hand operand.
Tool:

# Equality operators
Rule A5-10-1 (required, implementation, automated)
A pointer to member virtual function shall only be tested for equality with
null-pointer-constant.
Tool:

# Logical AND operator
Rule M5-14-1 (required, implementation, automated)
The right hand operand of a logical &&, || operators shall not contain side
effects.
Tool:

# Conditional operator
Rule A5-16-1 (required, implementation, automated)
The ternary conditional operator shall not be used as a sub-expression.
Tool:

# Assignment and compound assignment operation
Rule M5-17-1 (required, implementation, non-automated)
The semantic equivalence between a binary operator and its assignment
operator form shall be preserved.
Tool:

# Comma operator
Rule M5-18-1 (required, implementation, automated)
The comma operator shall not be used.
Tool:

# Constant expression
Rule M5-19-1 (required, implementation, automated)
Evaluation of constant unsigned integer expressions shall not lead to
wrap-around.
Tool:

# Statements
# Expression statement
Rule M6-2-1 (required, implementation, automated)
Assignment operators shall not be used in sub-expressions.
Tool:

Rule A6-2-1 (required, implementation, automated)
Move and copy assignment operators shall either move or respectively
copy base classes and data members of a class, without any side effects.
Tool:

Rule A6-2-2 (required, implementation, automated)
Expression statements shall not be explicit calls to constructors of
temporary objects only.
Tool:

Rule M6-2-2 (required, implementation, partially automated)
Floating-point expressions shall not be directly or indirectly tested for
equality or inequality.
Tool:

Rule M6-2-3 (required, implementation, automated)
Before preprocessing, a null statement shall only occur on a line by itself; it
may be followed by a comment, provided that the first character following
the null statement is a white-space character.
Tool:

# Compound statement or block
Rule M6-3-1 (required, implementation, automated)
The statement forming the body of a switch, while, do ... while or for
statement shall be a compound statement.
Tool:

# Selection statements
Rule M6-4-1 (required, implementation, automated)
An if ( condition ) construct shall be followed by a compound statement.
The else keyword shall be followed by either a compound statement, or
another if statement.
Tool:

Rule M6-4-2 (required, implementation, automated)
All if ... else if constructs shall be terminated with an else clause.
Tool:

Rule M6-4-3 (required, implementation, automated)
A switch statement shall be a well-formed switch statement.
Tool:

Rule M6-4-4 (required, implementation, automated)
A switch-label shall only be used when the most closely-enclosing
compound statement is the body of a switch statement.
Tool:

Rule M6-4-5 (required, implementation, automated)
An unconditional throw or break statement shall terminate every non-empty
switch-clause.
Tool:

Rule M6-4-6 (required, implementation, automated)
The final clause of a switch statement shall be the default-clause.
Tool:

Rule M6-4-7 (required, implementation, automated)
The condition of a switch statement shall not have bool type.
Tool:

Rule A6-4-1 (required, implementation, automated)
A switch statement shall have at least two case-clauses, distinct from the
default label.
Tool:

# Iteration statements
Rule A6-5-1 (required, implementation, automated)
A for-loop that loops through all elements of the container and does not use
its loop-counter shall not be used.
Tool:

Rule A6-5-2 (required, implementation, automated)
A for loop shall contain a single loop-counter which shall not have
floating-point type.
Tool:

Rule M6-5-2 (required, implementation, automated)
If loop-counter is not modified by −− or ++, then, within condition, the
loop-counter shall only be used as an operand to <=, <, > or >=.
Tool:

Rule M6-5-3 (required, implementation, automated)
The loop-counter shall not be modified within condition or statement.
Tool:

Rule M6-5-4 (required, implementation, automated)
The loop-counter shall be modified by one of: −−, ++, − = n, or + = n;
where n remains constant for the duration of the loop.
Tool:

Rule M6-5-5 (required, implementation, automated)
A loop-control-variable other than the loop-counter shall not be modified
within condition or expression.
Tool:

Rule M6-5-6 (required, implementation, automated)
A loop-control-variable other than the loop-counter which is modified in
statement shall have type bool.
Tool:

Rule A6-5-3 (advisory, implementation, automated)
Do statements should not be used.
Tool:

Rule A6-5-4 (advisory, implementation, automated)
For-init-statement and expression should not perform actions other than
loop-counter initialization and modification.
Tool:

# Jump statements
Rule A6-6-1 (required, implementation, automated)
The goto statement shall not be used.
Tool:

Rule M6-6-1 (required, implementation, automated)
Any label referenced by a goto statement shall be declared in the same
block, or in a block enclosing the goto statement.
Tool:

Rule M6-6-2 (required, implementation, automated)
The goto statement shall jump to a label declared later in the same function
body.
Tool:

Rule M6-6-3 (required, implementation, automated)
The continue statement shall only be used within a well-formed for loop.
Tool:

# Declaration
# Specifiers
Rule A7-1-1 (required, implementation, automated)
Constexpr or const specifiers shall be used for immutable data declaration.
Tool:

Rule A7-1-2 (required, implementation, automated)
The constexpr specifier shall be used for values that can be determined at
compile time.
Tool:

Rule M7-1-2 (required, implementation, automated)
A pointer or reference parameter in a function shall be declared as pointer
to const or reference to const if the corresponding object is not modified.
Tool:

Rule A7-1-3 (required, implementation, automated)
CV-qualifiers shall be placed on the right hand side of the type that is a
typedef or a using name.
Tool:

Rule A7-1-4 (required, implementation, automated)
The register keyword shall not be used.
Tool:

Rule A7-1-5 (required, implementation, automated)
The auto specifier shall not be used apart from following cases: (1) to
declare that a variable has the same type as return type of a function call, (2)
to declare that a variable has the same type as initializer of non-fundamental
type, (3) to declare parameters of a generic lambda expression, (4) to
declare a function template using trailing return type syntax.
Tool:

Rule A7-1-6 (required, implementation, automated)
The typedef specifier shall not be used.
Tool:

Rule A7-1-7 (required, implementation, automated)
Each expression statement and identifier declaration shall be placed on a
separate line.
Tool:

Rule A7-1-8 (required, implementation, automated)
A non-type specifier shall be placed before a type specifier in a declaration.
Tool:

Rule A7-1-9 (required, implementation, automated)
A class, structure, or enumeration shall not be declared in the definition of
its type.
Tool:

# Enumeration declaration
Rule A7-2-1 (required, implementation, automated)
An expression with enum underlying type shall only have values
corresponding to the enumerators of the enumeration.
Tool:

Rule A7-2-2 (required, implementation, automated)
Enumeration underlying base type shall be explicitly defined.
Tool:

Rule A7-2-3 (required, implementation, automated)
Enumerations shall be declared as scoped enum classes.
Tool:

Rule A7-2-4 (required, implementation, automated)
In an enumeration, either (1) none, (2) the first or (3) all enumerators shall be
initialized.
Tool:

Rule A7-2-5 (advisory, design, non-automated)
Enumerations should be used to represent sets of related named constants.
Tool:

# Namespaces
Rule M7-3-1 (required, implementation, automated)
The global namespace shall only contain main, namespace declarations
and extern "C" declarations.
Tool:

Rule M7-3-2 (required, implementation, automated)
The identifier main shall not be used for a function other than the global
function main.
Tool:

Rule M7-3-3 (required, implementation, automated)
There shall be no unnamed namespaces in header files.
Tool:

Rule M7-3-4 (required, implementation, automated)
Using-directives shall not be used.
Tool:

Rule A7-3-1 (required, implementation, automated)
All overloads of a function shall be visible from where it is called.
Tool:

Rule M7-3-6 (required, implementation, automated)
Using-directives and using-declarations (excluding class scope or function
scope using-declarations) shall not be used in header files.
Tool:

# The asm declaration
Rule A7-4-1 (required, implementation, automated)
The asm declaration shall not be used.
Tool:

Rule M7-4-1 (required, implementation, non-automated)
All usage of assembler shall be documented.
Tool:

Rule M7-4-2 (required, implementation, automated)
Assembler instructions shall only be introduced using the asm declaration.
Tool:

Rule M7-4-3 (required, implementation, automated)
Assembly language shall be encapsulated and isolated.
Tool:

# Linkage specification
Rule M7-5-1 (required, implementation, non-automated)
A function shall not return a reference or a pointer to an automatic variable
(including parameters), defined within the function.
Tool:

Rule M7-5-2 (required, implementation, non-automated)
The address of an object with automatic storage shall not be assigned to
another object that may persist after the first object has ceased to exist.
Tool:

Rule A7-5-1 (required, implementation, automated)
A function shall not return a reference or a pointer to a parameter that is
passed by reference to const.
Tool:

Rule A7-5-2 (required, implementation, automated)
Functions shall not call themselves, either directly or indirectly.
Tool:

# Attributes
Rule A7-6-1 (required, implementation, automated)
Functions declared with the [[noreturn]] attribute shall not return.
Tool:

# Declarators
# General
Rule M8-0-1 (required, implementation, automated)
An init-declarator-list or a member-declarator-list shall consist of a single
init-declarator or member-declarator respectively.
Tool:

# Ambiguity resolution
Rule A8-2-1 (required, implementation, automated)
When declaring function templates, the trailing return type syntax shall be
used if the return type depends on the type of parameters.
Tool:

# Meaning of declarators
Rule M8-3-1 (required, implementation, automated)
Parameters in an overriding virtual function shall either use the same
default arguments as the function they override, or else shall not specify
any default arguments.
Tool:

# Function definitions
Rule A8-4-1 (required, implementation, automated)
Functions shall not be defined using the ellipsis notation.
Tool:

Rule M8-4-2 (required, implementation, automated)
The identifiers used for the parameters in a re-declaration of a function shall
be identical to those in the declaration.
Tool:

Rule A8-4-2 (required, implementation, automated)
All exit paths from a function with non-void return type shall have an explicit
return statement with an expression.
Tool:

Rule M8-4-4 (required, implementation, automated)
A function identifier shall either be used to call the function or it shall be
preceded by &.
Tool:

Rule A8-4-3 (advisory, design, non-automated)
Common ways of passing parameters should be used.
Tool:

Rule A8-4-4 (advisory, design, automated)
Multiple output values from a function should be returned as a struct or
tuple.
Tool:

Rule A8-4-5 (required, design, automated)
”consume” parameters declared as X && shall always be moved from.
Tool:

Rule A8-4-6 (required, design, automated)
”forward” parameters declared as T && shall always be forwarded.
Tool:

Rule A8-4-7 (required, design, automated)
”in” parameters for ”cheap to copy” types shall be passed by value.
Tool:

Rule A8-4-8 (required, design, automated)
Output parameters shall not be used.
Tool:

Rule A8-4-9 (required, design, automated)
”in-out” parameters declared as T & shall be modified.
Tool:

Rule A8-4-10 (required, design, automated)
A parameter shall be passed by reference if it can’t be NULL
Tool:

Rule A8-4-11 (required, design, automated)
A smart pointer shall only be used as a parameter type if it expresses
lifetime semantics
Tool:

Rule A8-4-12 (required, design, automated)
A std::unique_ptr shall be passed to a function as: (1) a copy to express the
function assumes ownership (2) an lvalue reference to express that the
function replaces the managed object.
Tool:

Rule A8-4-13 (required, design, automated)
A std::shared_ptr shall be passed to a function as: (1) a copy to express the
function shares ownership (2) an lvalue reference to express that the
function replaces the managed object (3) a const lvalue reference to
express that the function retains a reference count.
Tool:

Rule A8-4-14 (required, design, non-automated)
Interfaces shall be precisely and strongly typed.
Tool:

# Initializers
Rule A8-5-0 (required, implementation, automated)
All memory shall be initialized before it is read.
Tool:

Rule A8-5-1 (required, implementation, automated)
In an initialization list, the order of initialization shall be following: (1) virtual
base classes in depth and left to right order of the inheritance graph, (2)
direct base classes in left to right order of inheritance list, (3) non-static
data members in the order they were declared in the class definition.
Tool:

Rule M8-5-2 (required, implementation, automated)
Braces shall be used to indicate and match the structure in the non-zero
initialization of arrays and structures.
Tool:

Rule A8-5-2 (required, implementation, automated)
Braced-initialization {}, without equals sign, shall be used for variable
initialization.
Tool:

Rule A8-5-3 (required, implementation, automated)
A variable of type auto shall not be initialized using {} or ={}
braced-initialization.
Tool:

Rule A8-5-4 (advisory, implementation, automated)
If a class has a user-declared constructor that takes a parameter of type
std::initializer_list, then it shall be the only constructor apart from special
member function constructors.
Tool:

# Classes
# Member function
Rule M9-3-1 (required, implementation, automated)
Const member functions shall not return non-const pointers or references
to class-data.
Tool:

Rule A9-3-1 (required, implementation, partially automated)
Member functions shall not return non-const “raw” pointers or references
to private or protected data owned by the class.
Tool:

Rule M9-3-3 (required, implementation, automated)
If a member function can be made static then it shall be made static,
otherwise if it can be made const then it shall be made const.
Tool:

# Unions
Rule A9-5-1 (required, implementation, automated)
Unions shall not be used.
Tool:

# Bit-fields
Rule M9-6-1 (required, implementation, non-automated)
When the absolute positioning of bits representing a bit-field is required,
then the behavior and packing of bit-fields shall be documented.
Tool:

Rule A9-6-1 (required, design, partially automated)
Data types used for interfacing with hardware or conforming to
communication protocols shall be trivial, standard-layout and only contain
members of types with defined sizes.
Tool:

Rule A9-6-2 (required, design, non-automated)
Bit-fields shall be used only when interfacing to hardware or conforming to
communication protocols.
Tool:

Rule M9-6-4 (required, implementation, automated)
Named bit-fields with signed integer type shall have a length of more than
one bit.
Tool:

# Derived Classes
# General
Rule A10-0-1 (required, design, non-automated)
Public inheritance shall be used to implement “is-a” relationship.
Tool:

Rule A10-0-2 (required, design, non-automated)
Membership or non-public inheritance shall be used to implement “has-a”
relationship.
Tool:

# Multiple base Classes
Rule A10-1-1 (required, implementation, automated)
Class shall not be derived from more than one base class which is not an
interface class.
Tool:

Rule M10-1-1 (advisory, implementation, automated)
Classes should not be derived from virtual bases.
Tool:

Rule M10-1-2 (required, implementation, automated)
A base class shall only be declared virtual if it is used in a diamond
hierarchy.
Tool:

Rule M10-1-3 (required, implementation, automated)
An accessible base class shall not be both virtual and non-virtual in the
same hierarchy.
Tool:

# Member name lookup
Rule M10-2-1 (advisory, implementation, automated)
All accessible entity names within a multiple inheritance hierarchy should
be unique.
Tool:

Rule A10-2-1 (required, implementation, automated)
Non-virtual public or protected member functions shall not be redefined in
derived classes.
Tool:

# Virtual functions
Rule A10-3-1 (required, implementation, automated)
Virtual function declaration shall contain exactly one of the three specifiers:
(1) virtual, (2) override, (3) final.
Tool:

Rule A10-3-2 (required, implementation, automated)
Each overriding virtual function shall be declared with the override or final
specifier.
Tool:

Rule A10-3-3 (required, implementation, automated)
Virtual functions shall not be introduced in a final class.
Tool:

Rule A10-3-5 (required, implementation, automated)
A user-defined assignment operator shall not be virtual.
Tool:

Rule M10-3-3 (required, implementation, automated)
A virtual function shall only be overridden by a pure virtual function if it is
itself declared as pure virtual.
Tool:

# Abstract Classes
Rule A10-4-1 (advisory, design, non-automated)
Hierarchies should be based on interface classes.
Tool:

# Member access control
# General
Rule M11-0-1 (required, implementation, automated)
Member data in non-POD class types shall be private.
Tool:

Rule A11-0-1 (advisory, implementation, automated)
A non-POD type should be defined as class.
Tool:

Rule A11-0-2 (required, implementation, automated)
A type defined as struct shall: (1) provide only public data members, (2) not
provide any special member functions or methods, (3) not be a base of
another struct or class, (4) not inherit from another struct or class.
Tool:

# Friends
Rule A11-3-1 (required, implementation, automated)
Friend declarations shall not be used.
Tool:

# Special member functions
# General
Rule A12-0-1 (required, implementation, automated)
If a class declares a copy or move operation, or a destructor, either via
“=default”, “=delete”, or via a user-provided declaration, then all others of
these five special member functions shall be declared as well.
Tool:

Rule A12-0-2 (required, implementation, partially automated)
Bitwise operations and operations that assume data representation in
memory shall not be performed on objects.
Tool:

# Constructors
Rule A12-1-1 (required, implementation, automated)
Constructors shall explicitly initialize all virtual base classes, all direct
non-virtual base classes and all non-static data members.
Tool:

Rule M12-1-1 (required, implementation, automated)
An object’s dynamic type shall not be used from the body of its constructor
or destructor.
Tool:

Rule A12-1-2 (required, implementation, automated)
Both NSDMI and a non-static member initializer in a constructor shall not be
used in the same type.
Tool:

Rule A12-1-3 (required, implementation, automated)
If all user-defined constructors of a class initialize data members with
constant values that are the same across all constructors, then data
members shall be initialized using NSDMI instead.
Tool:

Rule A12-1-4 (required, implementation, automated)
All constructors that are callable with a single argument of fundamental
type shall be declared explicit.
Tool:

Rule A12-1-5 (required, implementation, partially automated)
Common class initialization for non-constant members shall be done by a
delegating constructor.
Tool:

Rule A12-1-6 (required, implementation, automated)
Derived classes that do not need further explicit initialization and require all
the constructors from the base class shall use inheriting constructors.
Tool:

# Destructors
Rule A12-4-1 (required, implementation, automated)
Destructor of a base class shall be public virtual, public override or
protected non-virtual.
Tool:

Rule A12-4-2 (advisory, implementation, automated)
If a public destructor of a class is non-virtual, then the class should be
declared final.
Tool:

# Initialization
Rule A12-6-1 (required, implementation, automated)
All class data members that are initialized by the constructor shall be
initialized using member initializers.
Tool:

# Construction and destructions
Rule A12-7-1 (required, implementation, automated)
If the behavior of a user-defined special member function is identical to
implicitly defined special member function, then it shall be defined
“=default” or be left undefined.
Tool:

# Copying and moving class objects
Rule A12-8-1 (required, implementation, automated)
Move and copy constructors shall move and respectively copy base classes
and data members of a class, without any side effects.
Tool:

Rule A12-8-2 (advisory, implementation, automated)
User-defined copy and move assignment operators should use user-defined
no-throw swap function.
Tool:

Rule A12-8-3 (required, implementation, partially automated)
Moved-from object shall not be read-accessed.
Tool:

Rule A12-8-4 (required, implementation, automated)
Move constructor shall not initialize its class members and base classes
using copy semantics.
Tool:

Rule A12-8-5 (required, implementation, automated)
A copy assignment and a move assignment operators shall handle
self-assignment.
Tool:

Rule A12-8-6 (required, implementation, automated)
Copy and move constructors and copy assignment and move assignment
operators shall be declared protected or defined “=delete” in base class.
Tool:

Rule A12-8-7 (advisory, implementation, automated)
Assignment operators should be declared with the ref-qualifier &.
Tool:

# Overloading
# Overloadable declarations
Rule A13-1-2 (required, implementation, automated)
User defined suffixes of the user defined literal operators shall start with
underscore followed by one or more letters.
Tool:

Rule A13-1-3 (required, implementation, automated)
User defined literals operators shall only perform conversion of passed
parameters.
Tool:

# Declaration matching
Rule A13-2-1 (required, implementation, automated)
An assignment operator shall return a reference to “this”.
Tool:

Rule A13-2-2 (required, implementation, automated)
A binary arithmetic operator and a bitwise operator shall return a “prvalue”.
Tool:

Rule A13-2-3 (required, implementation, automated)
A relational operator shall return a boolean value.
Tool:

# Overload resolution
Rule A13-3-1 (required, implementation, automated)
A function that contains “forwarding reference” as its argument shall not be
overloaded.
Tool:

# Overloaded operators
Rule A13-5-1 (required, implementation, automated)
If “operator[]” is to be overloaded with a non-const version, const version
shall also be implemented.
Tool:

Rule A13-5-2 (required, implementation, automated)
All user-defined conversion operators shall be defined explicit.
Tool:

Rule A13-5-3 (advisory, implementation, automated)
User-defined conversion operators should not be used.
Tool:

Rule A13-5-4 (required, implementation, automated)
If two opposite operators are defined, one shall be defined in terms of the
other.
Tool:

Rule A13-5-5 (required, implementation, automated)
Comparison operators shall be non-member functions with identical
parameter types and noexcept.
Tool:

# Build-in operators
Rule A13-6-1 (required, implementation, automated)
Digit sequences separators ’ shall only be used as follows: (1) for decimal,
every 3 digits, (2) for hexadecimal, every 2 digits, (3) for binary, every 4
digits.
Tool:

# Templates
# General
# Template parameters
Rule A14-1-1 (advisory, implementation, non-automated)
A template should check if a specific template argument is suitable for this
template.
Tool:

# Template declarations
Rule A14-5-1 (required, implementation, automated)
A template constructor shall not participate in overload resolution for a
single argument of the enclosing class type.
Tool:

Rule A14-5-2 (advisory, design, partially-automated)
Class members that are not dependent on template class parameters
should be defined in a separate base class.
Tool:

Rule A14-5-3 (advisory, design, automated)
A non-member generic operator shall only be declared in a namespace that
does not contain class (struct) type, enum type or union type declarations.
Tool:

Rule M14-5-3 (required, implementation, automated)
A copy assignment operator shall be declared when there is a template
assignment operator with a parameter that is a generic parameter.
Tool:

# Name resolution                                                                   ********************************************************
Rule M14-6-1 (required, implementation, automated)
In a class template with a dependent base, any name that may be found in
that dependent base shall be referred to using a qualified-id or this->.
Tool: None

# Template instantiation and specialization
Rule A14-7-1 (required, implementation, automated)
A type used as a template argument shall provide all members that are used
by the template.
Tool: Compiler

Rule A14-7-2 (required, implementation, automated)
Template specialization shall be declared in the same file (1) as the primary
template (2) as a user-defined type, for which the specialization is declared.
Tool: None

# Function template specializations
Rule A14-8-2 (required, implementation, automated)
Explicit specializations of function templates shall not be used.
Tool: None

# Preprocessing directives
# General
Rule A16-0-1 (required, implementation, automated)
The pre-processor shall only be used for unconditional and conditional file
inclusion and include guards, and using the following directives: (1) #ifndef,
(2) #ifdef, (3) #if, (4) #if defined, (5) #elif, (6) #else, (7) #define, (8) #endif, (9)
#include.
Tool:

Rule M16-0-1 (required, implementation, automated)
#include directives in a file shall only be preceded by other pre-processor
directives or comments.
Tool:

Rule M16-0-2 (required, implementation, automated)
Macros shall only be #define’d or #undef’d in the global namespace.
Tool: clang -Wbuiltin-macro-redefined

Rule M16-0-5 (required, implementation, automated)
Arguments to a function-like macro shall not contain tokens that look like
pre-processing directives.
Tool:

Rule M16-0-6 (required, implementation, automated)
In the definition of a function-like macro, each instance of a parameter shall
be enclosed in parentheses, unless it is used as the operand of # or ##.
Tool: clang-tidy bugprone-macro-parentheses

Rule M16-0-7 (required, implementation, automated)
Undefined macro identifiers shall not be used in #if or #elif pre-processor
directives, except as operands to the defined operator.
Tool:

Rule M16-0-8 (required, implementation, automated)
If the # token appears as the first token on a line, then it shall be
immediately followed by a pre-processing token.
Tool:

# Conditional inclusion
Rule M16-1-1 (required, implementation, automated)
The defined pre-processor operator shall only be used in one of the two
standard forms.
defined ( identifier )
defined identifier
Tool: Compiler

Rule M16-1-2 (required, implementation, automated)
All #else, #elif and #endif pre-processor directives shall reside in the same
file as the #if or #ifdef directive to which they are related.
Tool: None

# Source file inclusion
Rule M16-2-3 (required, implementation, automated)
Include guards shall be provided.
Tool: clang-tidy llvm-header-guard

Rule A16-2-1 (required, implementation, automated)
The ’, ", /*, //, \ characters shall not occur in a header file name or in
#include directive.
Tool: None

Rule A16-2-2 (required, implementation, automated)
There shall be no unused include directives.
Tool: CLion Unused code | Unused include directive

Rule A16-2-3 (required, implementation, non-automated)
An include directive shall be added explicitly for every symbol used in a file.
Tool: 

# Macro replacement
Rule M16-3-1 (required, implementation, automated)
There shall be at most one occurrence of the # or ## operators in a single
macro definition.
Tool: None

Rule M16-3-2 (advisory, implementation, automated)
The # and ## operators should not be used.
Tool: None

# Error directive
Rule A16-6-1 (required, implementation, automated)
#error directive shall not be used.
Tool: None

# Pragma directive
The #pragma directive shall not be used.
Tool: None


References:
Clang: https://clang.llvm.org/docs/DiagnosticsReference.html
Clang-Tidy: https://clang.llvm.org/extra/clang-tidy/checks/list.html
