# ARM GNU Toolchain
# Using xPack since it makes the image much smaller.
# Toolchain:
# - git
# - arm-none-eabi
# - CMake
# - Ninja
FROM ubuntu:20.04 AS build

ENV DEBIAN_FRONTEND noninteractive
RUN apt update && apt install -y wget git

# Download xPack dependencies
ENV NODE_VERSION=18.14.2
ENV PATH "$PATH:node-v${NODE_VERSION}-linux-x64/bin/"
RUN wget https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz && \
    tar -xzvf node-v${NODE_VERSION}-linux-x64.tar.gz
RUN npm --version

# Install xPack
# Version available here: https://www.npmjs.com/package/xpm?activeTab=versions
ENV XPM_VERSION=0.14.9
RUN npm install --global xpm@${XPM_VERSION}

# Create folder with all xPack tools
ENV TOOLCHAIN=/toolchain
RUN mkdir ${TOOLCHAIN}

# Install ARM GNU Toolchain
# Version available here: https://www.npmjs.com/package/@xpack-dev-tools/arm-none-eabi-gcc?activeTab=versions
ENV GCC_VERSION=11.2.1-1.2.2
RUN xpm install @xpack-dev-tools/arm-none-eabi-gcc@${GCC_VERSION} --global
RUN mv /root/.local/xPacks/\@xpack-dev-tools/arm-none-eabi-gcc ${TOOLCHAIN}/arm-none-eabi-gcc
RUN ln -s ${TOOLCHAIN}/arm-none-eabi-gcc/${GCC_VERSION}/.content/bin/* /usr/bin
RUN ln -s ${TOOLCHAIN}/arm-none-eabi-gcc/${GCC_VERSION}/.content/bin/arm-none-eabi-gcc /usr/bin/cc
RUN ln -s ${TOOLCHAIN}/arm-none-eabi-gcc/${GCC_VERSION}/.content/bin/arm-none-eabi-g++ /usr/bin/c++
RUN ln -s ${TOOLCHAIN}/arm-none-eabi-gcc/${GCC_VERSION}/.content/bin/arm-none-eabi-gdb /usr/bin/gdb
RUN cc --version
RUN c++ --version
RUN gdb --version

# Install CMake
# Version available here: https://www.npmjs.com/package/@xpack-dev-tools/cmake?activeTab=versions
ENV CMAKE_VERSION=3.23.5-1.1
RUN xpm install @xpack-dev-tools/cmake@${CMAKE_VERSION} --global
RUN mv /root/.local/xPacks/\@xpack-dev-tools/cmake ${TOOLCHAIN}/cmake
RUN ln -s ${TOOLCHAIN}/cmake/${CMAKE_VERSION}/.content/bin/* /usr/bin
RUN cmake --version

# Install Ninja
# Version available here: https://www.npmjs.com/package/@xpack-dev-tools/ninja-build?activeTab=versions
ENV NINJA_VERSION=1.11.1-2.1
RUN xpm install @xpack-dev-tools/ninja-build@${NINJA_VERSION} --global
RUN mv /root/.local/xPacks/\@xpack-dev-tools/ninja-build ${TOOLCHAIN}/ninja-build
RUN ln -s ${TOOLCHAIN}/ninja-build/${NINJA_VERSION}/.content/bin/* /usr/bin
RUN ninja --version

# Cleanup
RUN npm uninstall --global xpm
RUN rm -rf node-v${NODE_VERSION}-linux-x64.tar.gz node-v${NODE_VERSION}-linux-x64 /root/.local/xPacks
ENV PATH "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

# Compress image
FROM scratch
COPY --from=build / /
