FROM rust:bullseye

ENV DEBIAN_FRONTEND noninteractive

RUN apt update && apt install -y jq
RUN rustup component add llvm-tools-preview clippy rustfmt
RUN ln -s /usr/local/rustup/toolchains/1.69.0-x86_64-unknown-linux-gnu/lib/rustlib/x86_64-unknown-linux-gnu/bin/llvm-profdata /usr/bin/llvm-profdata
RUN ln -s /usr/local/rustup/toolchains/1.69.0-x86_64-unknown-linux-gnu/lib/rustlib/x86_64-unknown-linux-gnu/bin/llvm-cov /usr/bin/llvm-cov
