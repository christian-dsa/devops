FROM rust:bullseye

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y mingw-w64 wine64
RUN rustup target add x86_64-pc-windows-gnu
