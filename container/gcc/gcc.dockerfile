# ARM GNU Toolchain
# Using xPack since it makes the image much smaller.
# Toolchain:
# - git
# - gcovr
# - gcc
# - CMake
# - Ninja
# - Catch2
FROM ubuntu:20.04 AS build

ENV DEBIAN_FRONTEND noninteractive
RUN apt update && apt install -y wget git libc6-dev python3-pip

# Install gcovr
RUN pip3 install gcovr

# Download xPack dependencies
ENV NODE_VERSION=18.14.2
ENV PATH "$PATH:node-v${NODE_VERSION}-linux-x64/bin/"
RUN wget https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz && \
    tar -xzvf node-v${NODE_VERSION}-linux-x64.tar.gz
RUN npm --version

# Install xPack
# Version available here: https://www.npmjs.com/package/xpm?activeTab=versions
ENV XPM_VERSION=0.14.9
RUN npm install --global xpm@${XPM_VERSION}

# Create folder with all xPack tools
ENV TOOLCHAIN=/toolchain
RUN mkdir ${TOOLCHAIN}

# Install GNU Toolchain
# Version available here: https://www.npmjs.com/package/@xpack-dev-tools/gcc?activeTab=versions
ENV GCC_VERSION=11.2.0-3.3
RUN xpm install @xpack-dev-tools/gcc@${GCC_VERSION} --global
RUN mv /root/.local/xPacks/\@xpack-dev-tools/gcc ${TOOLCHAIN}/gcc
RUN rm -rf \
    /usr/bin/addr2line \
    /usr/bin/ar \
    /usr/bin/as \
    /usr/bin/c++ \
    /usr/bin/c++filt \
    /usr/bin/cpp \
    /usr/bin/dwp \
    /usr/bin/elfedit \
    /usr/bin/g++ \
    /usr/bin/gcc \
    /usr/bin/gcc-ar \
    /usr/bin/gcc-nm \
    /usr/bin/gcc-ranlib \
    /usr/bin/gcov \
    /usr/bin/gcov-dump \
    /usr/bin/gcov-tool \
    /usr/bin/gprof \
    /usr/bin/ld \
    /usr/bin/ld.bfd \
    /usr/bin/ld.gold \
    /usr/bin/nm \
    /usr/bin/objcopy \
    /usr/bin/objdump \
    /usr/bin/ranlib \
    /usr/bin/readelf \
    /usr/bin/size \
    /usr/bin/strings \
    /usr/bin/strip \
    /usr/include/c++ \
    /lib/bfd-plugins \
    /lib/gcc
RUN ln -s ${TOOLCHAIN}/gcc/${GCC_VERSION}/.content/bin/* /usr/bin
RUN ln -s ${TOOLCHAIN}/gcc/${GCC_VERSION}/.content/include/* /usr/include
RUN ln -s ${TOOLCHAIN}/gcc/${GCC_VERSION}/.content/lib/* /lib/
RUN ln -s ${TOOLCHAIN}/gcc/${GCC_VERSION}/.content/lib64/* /lib64/
RUN ln -s ${TOOLCHAIN}/gcc/${GCC_VERSION}/.content/share/doc/* /usr/share/doc
RUN ln -s ${TOOLCHAIN}/gcc/${GCC_VERSION}/.content/share/gdb/* /usr/share/gdb
RUN ln -s ${TOOLCHAIN}/gcc/${GCC_VERSION}/.content/share/gcc* /usr/share
RUN gcc --version
RUN g++ --version
RUN gdb --version

# FIX GLIBC Version
RUN rm /usr/lib/x86_64-linux-gnu/libstdc++.so.6
RUN ln -s /usr/lib64/libstdc++.so.6 /usr/lib/x86_64-linux-gnu/libstdc++.so.6

# Install CMake
# Version available here: https://www.npmjs.com/package/@xpack-dev-tools/cmake?activeTab=versions
ENV CMAKE_VERSION=3.23.5-1.1
RUN xpm install @xpack-dev-tools/cmake@${CMAKE_VERSION} --global
RUN mv /root/.local/xPacks/\@xpack-dev-tools/cmake ${TOOLCHAIN}/cmake
RUN ln -s ${TOOLCHAIN}/cmake/${CMAKE_VERSION}/.content/bin/* /usr/bin
RUN cmake --version

# Install Ninja
# Version available here: https://www.npmjs.com/package/@xpack-dev-tools/ninja-build?activeTab=versions
ENV NINJA_VERSION=1.11.1-2.1
RUN xpm install @xpack-dev-tools/ninja-build@${NINJA_VERSION} --global
RUN mv /root/.local/xPacks/\@xpack-dev-tools/ninja-build ${TOOLCHAIN}/ninja-build
RUN ln -s ${TOOLCHAIN}/ninja-build/${NINJA_VERSION}/.content/bin/* /usr/bin
RUN ninja --version

# Install Catch2 v2.
ENV CATCH2_VERSION=2.13.10
RUN wget https://github.com/catchorg/Catch2/archive/refs/tags/v${CATCH2_VERSION}.tar.gz
RUN tar -xzvf v${CATCH2_VERSION}.tar.gz
RUN cd Catch2-${CATCH2_VERSION} && \
    cmake -Bbuild -H. -DBUILD_TESTING=OFF -G Ninja && \
    cmake --build build/ --target install

# Cleanup
RUN npm uninstall --global xpm
RUN rm -rf node-v${NODE_VERSION}-linux-x64.tar.gz node-v${NODE_VERSION}-linux-x64 /root/.local/xPacks
ENV PATH "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

# Compress image
FROM scratch
COPY --from=build / /

