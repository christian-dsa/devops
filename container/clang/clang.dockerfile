# ARM GNU Toolchain
# Using xPack since it makes the image much smaller.
# Toolchain:
# - git
# - gcovr
# - clang
# - CMake
# - Ninja
# - Catch2
FROM ubuntu:22.04 AS build

ENV DEBIAN_FRONTEND noninteractive
RUN apt update && apt install -y wget git python3-pip build-essential g++ gdb libunwind-13-dev

# Install gcovr
RUN pip3 install gcovr

# Download xPack dependencies
ENV NODE_VERSION=18.14.2
ENV PATH "$PATH:node-v${NODE_VERSION}-linux-x64/bin/"
RUN wget https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz && \
    tar -xzvf node-v${NODE_VERSION}-linux-x64.tar.gz
RUN npm --version

# Install xPack
# Version available here: https://www.npmjs.com/package/xpm?activeTab=versions
ENV XPM_VERSION=0.14.9
RUN npm install --global xpm@${XPM_VERSION}

# Create folder with all xPack tools
ENV TOOLCHAIN=/toolchain
RUN mkdir ${TOOLCHAIN}

# Install Clang
# Version available here: https://www.npmjs.com/package/@xpack-dev-tools/clang?activeTab=versions
ENV CLANG_VERSION=15.0.7-2.1
RUN xpm install @xpack-dev-tools/clang@${CLANG_VERSION} --global
RUN mv /root/.local/xPacks/\@xpack-dev-tools/clang ${TOOLCHAIN}/clang
RUN rm -rf ${TOOLCHAIN}/clang/${CLANG_VERSION}/.content/bin/dwp \
    ${TOOLCHAIN}/clang/${CLANG_VERSION}/.content/bin/ld.gold \
    ${TOOLCHAIN}/clang/${CLANG_VERSION}/.content/include/c++
RUN ln -s ${TOOLCHAIN}/clang/${CLANG_VERSION}/.content/bin/* /usr/bin/
RUN ln -s ${TOOLCHAIN}/clang/${CLANG_VERSION}/.content/include/* /usr/include/
RUN ln -s ${TOOLCHAIN}/clang/${CLANG_VERSION}/.content/lib/* /usr/lib/
RUN ln -s ${TOOLCHAIN}/clang/${CLANG_VERSION}/.content/share/* /usr/share/
RUN ln -s ${TOOLCHAIN}/clang/${CLANG_VERSION}/.content/libexec/* /usr/libexec/
RUN clang-format --version
RUN clang-tidy --version

# Install CMake
# Version available here: https://www.npmjs.com/package/@xpack-dev-tools/cmake?activeTab=versions
ENV CMAKE_VERSION=3.23.5-1.1
RUN xpm install @xpack-dev-tools/cmake@${CMAKE_VERSION} --global
RUN mv /root/.local/xPacks/\@xpack-dev-tools/cmake ${TOOLCHAIN}/cmake
RUN ln -s ${TOOLCHAIN}/cmake/${CMAKE_VERSION}/.content/bin/* /usr/bin
RUN cmake --version

# Install Ninja
# Version available here: https://www.npmjs.com/package/@xpack-dev-tools/ninja-build?activeTab=versions
ENV NINJA_VERSION=1.11.1-2.1
RUN xpm install @xpack-dev-tools/ninja-build@${NINJA_VERSION} --global
RUN mv /root/.local/xPacks/\@xpack-dev-tools/ninja-build ${TOOLCHAIN}/ninja-build
RUN ln -s ${TOOLCHAIN}/ninja-build/${NINJA_VERSION}/.content/bin/* /usr/bin
RUN ninja --version

# Install Catch2 v2.
ENV CATCH2_VERSION=2.13.10
RUN wget https://github.com/catchorg/Catch2/archive/refs/tags/v${CATCH2_VERSION}.tar.gz
RUN tar -xzvf v${CATCH2_VERSION}.tar.gz
RUN cd Catch2-${CATCH2_VERSION} && \
    cmake -Bbuild -H. -DBUILD_TESTING=OFF -G Ninja && \
    cmake --build build/ --target install

# Cleanup
RUN npm uninstall --global xpm
RUN rm -rf node-v${NODE_VERSION}-linux-x64.tar.gz node-v${NODE_VERSION}-linux-x64 /root/.local/xPacks
RUN rm -rf Catch2-${CATCH2_VERSION} v${CATCH2_VERSION}.tar.gz
ENV PATH "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

# Compress image
FROM scratch
COPY --from=build / /

